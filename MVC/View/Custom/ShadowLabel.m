//
//  ShadowLabel.m
//  Assignment
//
//  Created by fsociety on 9/29/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "ShadowLabel.h"

@implementation ShadowLabel
{
    CAShapeLayer *shadowLayer;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (self.borderWidth != 0 && self.borderColor != nil)
    {
        self.layer.borderWidth = self.borderWidth;
        self.layer.borderColor = self.borderColor.CGColor;
    }
    
    if (self.cornerRadius != 0)
    {
        self.layer.cornerRadius = self.cornerRadius;
        self.layer.masksToBounds = YES;
    }
    
    if (self.shadow)
    {
        CGMutablePathRef path = CGPathCreateMutable();
        CGRect shadowRect = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1);
        CGPathAddRect(path, NULL, shadowRect);
        
        if (!shadowLayer)
        {
            shadowLayer = [[CAShapeLayer alloc] init];
            shadowLayer.shadowOffset = self.shadowOffsetValue;
            shadowLayer.shadowColor = [UIColor grayColor].CGColor;
            shadowLayer.shadowRadius = 2;
            shadowLayer.shadowOpacity = 0.5;
            
            [self.layer insertSublayer:shadowLayer atIndex:0];
        }
        
        shadowLayer.shadowPath = path;
        shadowLayer.frame = self.bounds;
    }
}

@end
