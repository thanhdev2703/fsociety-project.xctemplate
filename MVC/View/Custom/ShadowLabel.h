//
//  ShadowLabel.h
//  Assignment
//
//  Created by fsociety on 9/29/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "BaseLabel.h"

@interface ShadowLabel : BaseLabel
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic,strong) IBInspectable UIColor * borderColor;
@property (nonatomic) IBInspectable BOOL shadow;
@property (nonatomic) IBInspectable CGSize shadowOffsetValue;
@end
