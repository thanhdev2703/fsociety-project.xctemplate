//
//  RoundRectView.h
//  Assign System
//
//  Created by fsociety on 8/25/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface RoundRectView : UIView
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic,strong) IBInspectable UIColor * borderColor;
@property (nonatomic) IBInspectable BOOL shadow;
@property (nonatomic) IBInspectable CGSize shadowOffset;
@end
