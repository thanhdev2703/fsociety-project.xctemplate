//
//  ShadowView.h
//  Assignment
//
//  Created by fsociety on 9/15/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import <UIKit/UIKit.h>
// IB_DESIGNABLE
@interface ShadowView : UIView
@property (nonatomic) IBInspectable BOOL shadow;
@property (nonatomic) IBInspectable CGSize shadowOffset;
@end
