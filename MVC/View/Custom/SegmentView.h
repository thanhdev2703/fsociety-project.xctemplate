//
//  SegmentView.h
//  Assignment
//
//  Created by fsociety on 9/15/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundRectView.h"
@protocol SegmentViewDelegate;
@interface SegmentView : RoundRectView
// Property
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic,weak) id<SegmentViewDelegate> delegate;
@property (nonatomic,strong) IBInspectable UIColor *selectedColor;
@property (nonatomic,strong) IBInspectable UIColor *normalColor;
@property (nonatomic,strong) IBInspectable UIColor *titleSelectedColor;
@property (nonatomic,strong) IBInspectable UIColor *titleNormalColor;
@property (nonatomic,strong) NSArray<NSString *> *segments;
// Outlet

// IBAction

// Public function

@end

@protocol SegmentViewDelegate <NSObject>
-(void) segmentView:(SegmentView *)segmentView didSelectAtIndex:(NSInteger)index;
@end
