//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "BaseButton.h"
#import "FontProvider.h"
@implementation BaseButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self adjustFont];
}

-(void) adjustFont
{
    UIFont *font = self.titleLabel.font;
    NSInteger fontSize = font.pointSize;
    
    if ([font.familyName isEqualToString:[UIFont systemFontOfSize:12].familyName])
    {
        if (font.fontDescriptor.symbolicTraits&UIFontDescriptorTraitBold)
        {
            self.titleLabel.font = [FontProvider boldFontWithSize:fontSize];
        }
        else
        {
            
            self.titleLabel.font = [FontProvider regularFontWithSize:fontSize];
        }
    }
}

@end
