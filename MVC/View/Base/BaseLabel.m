//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "BaseLabel.h"
#import "FontProvider.h"
@implementation BaseLabel

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self adjustFont];
}


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    return self;
}

-(void)setFont:(UIFont *)font
{
    [super setFont:font];
    
    [self adjustFont];
}

-(void) adjustFont
{
    UIFont *font = self.font;
    NSInteger fontSize = font.pointSize;
    
    if ([font.familyName isEqualToString:[UIFont systemFontOfSize:12].familyName])
    {
        if (font.fontDescriptor.symbolicTraits&UIFontDescriptorTraitBold)
        {
            self.font = [FontProvider boldFontWithSize:fontSize];
        }
        else
        {
            self.font = [FontProvider regularFontWithSize:fontSize];
        }
    }
}


@end
