//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
// Property

// Outlet

// IBAction

// Public function
@end
