//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "BaseViewController.h"

#define debug 0
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self config];
}

#pragma mark - Config

-(void) config
{
    
}

#pragma mark - LoadData

#pragma mark - IBAction

#pragma mark - Delegate

#pragma mark - Gesture

#pragma mark - Utils



@end
