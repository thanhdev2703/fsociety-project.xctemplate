//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

+(NSArray *) parseWithListDictionary:(NSArray *)list;
+(instancetype) parseWithDictionary:(NSDictionary *)dict;

@end
