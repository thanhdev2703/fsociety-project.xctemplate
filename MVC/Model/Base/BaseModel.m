//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "BaseModel.h"

@implementation BaseModel

+(NSArray *)parseWithListDictionary:(NSArray *)list
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    for (NSDictionary *item in list)
    {
        BaseModel *obj = [[self class] parseWithDictionary:item];
        
        [results addObject:obj];
    }
    
    return results;
}

+(instancetype)parseWithDictionary:(NSDictionary *)dict
{
    @throw @"Missing parse with dictionary function";
}

@end
