//
//  APIName.h
//  BeeSystem
//
//  Created by fsociety on 8/22/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#define kAuthAPI @"MStaffs/Authentication"
#define kAllStaffAPI @"Staffs"
#define kAllUserAPI @"MUserPatients/Mobile"
#define kAllStaffByCloudCode @"TDailyBusinessSimpleMailGroup/GetByCloudCode"
#define kCertifyMACAPI @"Staff/CertifyMAC"
#define kCertifyUDIDAPI @"Staff/CertifyUDID"
#define kCreateTokenKeyAPI @"Staff/CreateTokenKey"
#define kChangePwdAPI @"MStaffs/ChangePwd"
#define kRecordTemplateGroupsAPI @"MRecordTemplateGroups"
#define kElapsedTemplatesAPI @"MRecordTemplates"
#define kBathRecordsAPI @"RCBathRecords"
#define kExcretionRecordsAPI @"RCExcretionRecords"
#define kVitalRecordsAPI @"RCVitalRecords"
#define kMealMoistureRecordsAPI @"RCMealMoistureRecords"
#define kElapsedRecordsAPI @"RCElapsedRecords"

#define kVisitPlanTableAPI @"VPVisitPlanTable"
#define kUserOrderAPI @"ODUserOrder"
#define kUserServiceAPI @"USUserService/Mobile"
#define kUserFamilyContactAPI @"USUserFamilyContacts/Mobile"
#define kUserDoctorAPI @"USUserDoctors/Mobile"
#define kOrderGroupAPI @"MOrderGroups/Mobile"
#define kServiceTableAPI @"MServices/Mobile"
#define kStaffTableAPI @"MStaffs/Mobile"
#define kOrderCodeLstTableAPI @"MOrderCodesLst/Mobile"
#define kUserOrderDetailTableAPI @"ODUserOrderDetail/Mobile"
