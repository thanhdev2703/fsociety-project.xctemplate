//
//  INetworkRequester.h
//  BeeSystem
//
//  Created by fsociety on 8/21/17.
//  Copyright © 2017 OMI. All rights reserved.
//
#import "APIResult.h"
typedef void(^BSAPIComplete)(APIResult *apiResult);
typedef void(^BSAPIFailure)(APIErrorCode error);
#define VoidBlock(Params) void (^)(Params)

@protocol INetworkRequester <NSObject>
-(NSString *) baseURL;
-(void) requestWithURLRequest:(NSURLRequest *)request completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure;
-(void) requestWithURL:(NSURL *)url urlParams:(NSDictionary *)urlParams postData:(NSDictionary *)data completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure;
-(void) requestWithURLString:(NSString *)url urlParams:(NSDictionary *)urlParams postData:(NSDictionary *)data completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure;
-(void) requestWithAPI:(NSString *)apiName urlParams:(NSDictionary *)urlParams postData:(NSDictionary *)data completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure;
-(void) uploadImages:(NSArray *)images names:(NSArray *)names url:(NSString *)urlString completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure;
@end
