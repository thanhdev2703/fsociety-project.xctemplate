//
//  APIResult.h
//  BeeSystem
//
//  Created by fsociety on 8/21/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDictionary+Extensions.h"

typedef enum : NSUInteger {
    APIErrorCodeSuccess = 0,
    APIErrorCodeTimout = -999,
    APIErrorCodeDataStructError = -998,
    APIErrorCodeUsernameOrPasswordError = 1,
    APIErrorCodeDataNotfoundError = 3
} APIErrorCode;

NSString * NSStringFromAPIErrorCode(APIErrorCode code);
@interface APIResult : NSObject
@property (nonatomic) APIErrorCode errorCode;
@property (nonatomic,strong) NSDictionary *data;

+(APIResult *) parseWithDictionary:(NSDictionary *)dict;
@end
