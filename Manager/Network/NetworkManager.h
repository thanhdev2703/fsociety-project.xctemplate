//
//  NetworkManager.h
//  BaseProject
//
//  Created by Tinhvv on 8/12/16.
//  Copyright © 2016 Tinhvv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIResult.h"
#import "INetworkRequester.h"
#import "AppConfig.h"

@interface NetworkManager : NSObject<INetworkRequester>
@end
