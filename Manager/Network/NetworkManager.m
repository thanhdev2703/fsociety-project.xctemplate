//
//  NetworkManager.m
//  BaseProject
//
//  Created by Tinhvv on 8/12/16.
//  Copyright © 2016 Tinhvv. All rights reserved.
//

#import "NetworkManager.h"
#import "AppConfig.h"

@implementation NetworkManager
-(NSString *)baseURL
{
    BOOL isSetHTTPS = [[NSUserDefaults standardUserDefaults] boolForKey:@"kSetHTTPS"];
#ifdef STAGGING
//    kSetHTTPS
    
    if (isSetHTTPS == true)
    {
        NSString *baseURL = [NSString stringWithFormat:@"https:%@",kAppAPIBaseURLStagging];
        return baseURL;
    }
    else
    {
        NSString *baseURL = [NSString stringWithFormat:@"http:%@",kAppAPIBaseURLStagging];
        return baseURL;
    }
#endif
    
#ifdef PRODUCT
    
    if (isSetHTTPS == true)
    {
        NSString *baseURL = [NSString stringWithFormat:@"https:%@",kAppAPIBaseURLProduct];
        return baseURL;
    }
    else
    {
        NSString *baseURL = [NSString stringWithFormat:@"http:%@",kAppAPIBaseURLProduct];
        return baseURL;
    }
#endif

    if (isSetHTTPS == true)
    {
        NSString *baseURL = [NSString stringWithFormat:@"https:%@",kAppAPIBaseURLDev];
        return baseURL;
    }
    else
    {
        NSString *baseURL = [NSString stringWithFormat:@"http:%@",kAppAPIBaseURLDev];
        return baseURL;
    }
}

-(void)requestWithURLRequest:(NSURLRequest *)request completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure
{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 10;
    sessionConfig.timeoutIntervalForResource = 10;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error)
        {
            failure(APIErrorCodeTimout);
        }
        else
        {
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if (error)
            {
                failure(APIErrorCodeDataStructError);
            }
            else
            {
                APIResult *result = [APIResult parseWithDictionary:json];
                
                if (result.errorCode != APIErrorCodeSuccess)
                {
                    failure(result.errorCode);
                }
                else
                {
                    complete(result);
                }
            }
        }
        
        [session finishTasksAndInvalidate];
    }];
    
    [dataTask resume];
}

-(void)requestWithURL:(NSURL *)url urlParams:(NSDictionary *)urlParams postData:(NSDictionary *)data completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure
{
    if ([urlParams allKeys].count)
    {
        NSString *paramsQuery = [self urlParamsQueryStringWithDictionary:urlParams];
        
        NSString *urlString = url.absoluteString;
        urlString = [urlString stringByAppendingString:paramsQuery];
        urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        url = [NSURL URLWithString:urlString];
    }
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    if (data.allKeys.count)
    {
        urlRequest.HTTPMethod = @"POST";
        
        NSData *body = [self bodyStringWithDictionary:data];
        urlRequest.HTTPBody = body;
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    else
    {
        urlRequest.HTTPMethod = @"GET";
    }
    
    [self requestWithURLRequest:urlRequest completeHandler:complete failure:failure];
}

-(void)requestWithURLString:(NSString *)url urlParams:(NSDictionary *)urlParams postData:(NSDictionary *)data completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure
{
    [self requestWithURL:[NSURL URLWithString:url] urlParams:urlParams postData:data completeHandler:complete failure:failure];
}

-(void)requestWithAPI:(NSString *)apiName urlParams:(NSDictionary *)urlParams postData:(NSDictionary *)data completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure
{
    NSString *baseURL = [self baseURL];
    NSString *fullURLAPI = [baseURL stringByAppendingFormat:@"/%@",apiName];
    
    [self requestWithURLString:fullURLAPI urlParams:urlParams postData:data completeHandler:complete failure:failure];
}

#pragma mark - Form data

-(void)uploadImages:(NSArray *)images names:(NSArray *)names url:(NSString *)urlString completeHandler:(BSAPIComplete)complete failure:(BSAPIFailure)failure
{
    NSString *bound = @"_______Bee_______";
    
    NSMutableData *package = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", bound] forHTTPHeaderField:@"Content-Type"];
    
    for (UIImage *image in images)
    {
        NSData *imageData = UIImagePNGRepresentation(image);
        NSString *imageName = [names objectAtIndex:[images indexOfObject:image]];
        [package appendData:[[NSString stringWithFormat:@"--%@\r\n",bound] dataUsingEncoding:NSUTF8StringEncoding]];
        [package appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", imageName,imageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [package appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [package appendData:imageData];
        [package appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [package appendData:[[NSString stringWithFormat:@"--%@--",bound] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:package];
    
    [self requestWithURLRequest:request completeHandler:complete failure:failure];
}

#pragma mark - Utilities
-(NSString *) urlParamsQueryStringWithDictionary:(NSDictionary *)data
{
    if (data.allKeys.count == 0)
    {
        return @"";
    }
    
    NSString *resultString = @"?";
    for (NSString *key in data.allKeys)
    {
        NSString *value = [data objectForKey:key];
    
        if (data.allKeys.lastObject == key)
        {
            resultString = [resultString stringByAppendingFormat:@"%@=%@", key, value];
        }
        else
        {
            resultString = [resultString stringByAppendingFormat:@"%@=%@&", key, value];
        }
    }
    
    return resultString;
}

-(NSData *) bodyStringWithDictionary:(NSDictionary *)dict
{
    return [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
}


@end
