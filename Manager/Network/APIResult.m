//
//  APIResult.m
//  BeeSystem
//
//  Created by fsociety on 8/21/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "APIResult.h"

@implementation APIResult
+(APIResult *)parseWithDictionary:(NSDictionary *)dict
{
    APIResult *result = [[APIResult alloc] init];
    result.errorCode = [[dict objForKey:@"errorCode"] intValue];
    result.data = [dict objForKey:@"data"];
    
    return result;
}
@end

NSString * NSStringFromAPIErrorCode(APIErrorCode code)
{
    switch (code) {
        case APIErrorCodeTimout:
            return @"APIErrorCodeTimout";
            break;
        case APIErrorCodeSuccess:
            return @"APIErrorCodeSuccess";
            break;
        case APIErrorCodeDataStructError:
            return @"APIErrorCodeDataStructError";
            break;
        case APIErrorCodeUsernameOrPasswordError:
            return @"APIErrorCodeUsernameOrPasswordError";
            break;
        default:
            break;
    }
    return @"Unknow";
}
