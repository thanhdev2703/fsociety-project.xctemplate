//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "FontProvider.h"

@implementation FontProvider
+(UIFont *)regularFontWithSize:(NSInteger)size
{
    return [UIFont boldSystemFontOfSize:size];
}

+(UIFont *)boldFontWithSize:(NSInteger)size
{
    return [UIFont systemFontOfSize:size];
}

/*+(UIFontDescriptor *) regularFontDescriptor
{
    UIFontDescriptor *fallback = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorNameAttribute:@"HelveticaNeue"}];
    UIFontDescriptor *decriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorNameAttribute:@"Meiryo",UIFontDescriptorCascadeListAttribute:@[fallback]}];
    
    return decriptor;
}

+(UIFontDescriptor *) boldFontDescriptor
{
    UIFontDescriptor *fallback = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorNameAttribute:@"HelveticaNeue-Bold"}];
    UIFontDescriptor *decriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorNameAttribute:@"Meiryo-Bold",UIFontDescriptorCascadeListAttribute:@[fallback]}];
    
    return decriptor;
}*/

@end
