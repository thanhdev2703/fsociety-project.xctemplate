//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface FontProvider : NSObject
+(UIFont *) regularFontWithSize:(NSInteger)size;
+(UIFont *) boldFontWithSize:(NSInteger)size;
@end
