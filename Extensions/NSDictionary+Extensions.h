//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extensions)
-(id) objForKey:(id)aKey;
-(NSDictionary *) reverse;
@end
