//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "NSData+Extensions.h"

@implementation NSData (Extensions)
-(NSString *)hexString
{
    unsigned char * buffer = (unsigned char *)self.bytes;
    
    NSUInteger dataLength = self.length;
    NSMutableString *result = [[NSMutableString alloc] init];
    
    for (int i = 0; i < dataLength; i++)
    {
        unsigned char byte = buffer[i];
        
        [result appendFormat:@"%02lx",(unsigned long)byte];
    }
    
    return result;
}

+(NSData *)dataFromHexString:(NSString *)hexString
{
    NSMutableData *data = [[NSMutableData alloc] init];
    
    for (int i = 0; i < hexString.length; i+=2)
    {
        char first = [hexString characterAtIndex:i];
        char last = [hexString characterAtIndex:i+1];
        
        char * byteChar = malloc(2*sizeof(char));
        byteChar[0] = first;
        byteChar[1] = last;
        
        unsigned char byte = strtol(byteChar, NULL, 16);
        [data appendBytes:&byte length:1];
    }
    
    return data;
}
@end

