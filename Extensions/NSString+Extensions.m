//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "NSString+Extensions.h"

@implementation NSString (Extensions)
-(NSDate *)dateValue
{
    NSDate *date = [Utils dateFromTokyoDateTimeString:self];
    
    if (!date)
    {
        date = [Utils dateFromTokyoDateString:self];
    }
    
    return date;
}
@end
