//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (NSDateFomatter_Util)

/**
 @return fcp DBフォーマット yyyy-MM-dd'T'HH:mm:ss でフォーマット済みのNSDateformatter
 */
+(NSDateFormatter*)formatterWithDbFormat;

+(NSDateFormatter*)formatterJapanLocale;


@end
