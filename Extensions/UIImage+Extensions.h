//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <UIKit/UIKit.h>

@interface UIImage (Extensions)
-(NSString *)base64;
-(instancetype) initWithBase64String:(NSString *)base64String;
+(UIImage *) imageWithBase64String:(NSString *)base64String;
@end
