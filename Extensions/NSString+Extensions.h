//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>
#import "Utils.h"
@interface NSString (Extensions)

/**
 Get date value from string return from server/DB
 Format: yyyy-MM-dd'T'HH:mm:ss
 
 @return NSDate instance
 */
-(NSDate *) dateValue;
@end
