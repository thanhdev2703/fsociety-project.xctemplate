//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "NSDictionary+Extensions.h"

@implementation NSDictionary (Extensions)
-(id)objForKey:(id)aKey
{
    id result = [self objectForKey:aKey];
    if ([result isKindOfClass:[NSNull class]])
    {
        return nil;
    }
    
    return result;
}

-(NSDictionary *)reverse
{
    NSMutableDictionary *reverseDict = [[NSMutableDictionary alloc] init];
    for (NSString *key in self.allKeys)
    {
        NSString *value = [self objectForKey:key];
        
        [reverseDict setObject:key forKey:value];
    }
    
    return reverseDict;
}

-(NSString *)description
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
@end
