//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "NSDate+Extensions.h"
#import "Utils.h"
@implementation NSDate (Extensions)
-(NSString *)description
{
    return [Utils tokyoDatetimeStringFromDate:self];
}

-(NSString *)debugDescription
{
    return [Utils tokyoDatetimeStringFromDate:self];
}

-(NSDate *)firstHour
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:self];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    return date;
}

-(BOOL)isEqualDate:(NSDate *)date
{
    NSDateComponents *date1Components = [[NSCalendar currentCalendar] components:(NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self];
    NSDateComponents *date2Components = [[NSCalendar currentCalendar] components:(NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:date];
    
    if (date1Components.day == date2Components.day && date1Components.month == date2Components.month && date1Components.year == date2Components.year)
    {
        return YES;
    }
    
    return NO;
}

-(NSString *)dateKey
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self];
    
    return [NSString stringWithFormat:@"%02d%02d%04d",(int)components.day,(int)components.month,(int)components.year];
}

+(NSDate *)dateFromDateKey:(NSString *)dateKey
{
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"ddMMyyyy"];
    
    return [dateFormater dateFromString:dateKey];
}
@end
