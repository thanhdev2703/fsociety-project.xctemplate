//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#pragma once
#import <Foundation/Foundation.h>


@interface NSObject (NSObjectExtension)

/**
 * return all collectable properties of this object (including super classes' properties)
 * 
 */
- (NSDictionary*)propertiesDictionary;

/**
 * return all methods' names of this object
 * 
 */
- (NSArray*)methodsArray;

@end
