//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>

@interface NSData (Extensions)
-(NSString *) hexString;

+(NSData *) dataFromHexString:(NSString *)hexString;
@end
