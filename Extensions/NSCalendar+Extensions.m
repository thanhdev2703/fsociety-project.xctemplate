//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "NSCalendar+Extensions.h"

@implementation NSCalendar (Extensions)
+(NSArray<NSString *> *) weekDays
{
    return @[@"日",@"月",@"火",@"水",@"木",@"金",@"土"];
}
@end
