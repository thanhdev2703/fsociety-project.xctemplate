//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>

@interface NSDate (Extensions)
-(NSDate *) firstHour;
-(BOOL) isEqualDate:(NSDate *)date;
-(NSString *) dateKey;

+(NSDate *) dateFromDateKey:(NSString *)dateKey;
@end
