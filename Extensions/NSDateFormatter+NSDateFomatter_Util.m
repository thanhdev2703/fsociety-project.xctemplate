//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "NSDateFormatter+NSDateFomatter_Util.h"

@implementation NSDateFormatter (NSDateFomatter_Util)

+(NSDateFormatter*)formatterWithDbFormat{
    
    NSDateFormatter *dfm = [[NSDateFormatter alloc]init];
    [dfm setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
    [dfm setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"JST"]];
    [dfm setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    return dfm;
}

+(NSDateFormatter *)formatterJapanLocale{
    NSDateFormatter *dfm = [[NSDateFormatter alloc]init];
    [dfm setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
    [dfm setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"JST"]];
    [dfm setTimeStyle:NSDateFormatterLongStyle];
    return dfm;
}

@end
