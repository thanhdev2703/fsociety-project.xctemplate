//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import "Utils.h"
static NSDateFormatter *localDateFormater;
static NSDateFormatter *serverDateTimeFormater;
static NSDateFormatter *serverDateFormater;
@implementation Utils
void NSLog(NSString *format, ...)
{
#ifdef DEBUG
    va_list args;
    va_start(args, format);
    NSLogv(format, args);
    va_end(args);
#endif
}

+(void)load
{
    localDateFormater = [[NSDateFormatter alloc] init];
    [localDateFormater setDateFormat:@"dd/MM/yyyy"];
    
    serverDateTimeFormater = [[NSDateFormatter alloc] init];
    [serverDateTimeFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    [serverDateTimeFormater setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Tokyo"]];
    
    serverDateFormater = [[NSDateFormatter alloc] init];
    [serverDateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [serverDateFormater setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Tokyo"]];
}

#pragma mark - Date
+(NSString *)stringOnlyFromDate:(NSDate *)date
{
    return [localDateFormater stringFromDate:date];
}

+(NSDate *)dateFromDateString:(NSString *)dateString
{
    return [localDateFormater dateFromString:dateString];
}

+(NSDate *)dateFromTokyoDateTimeString:(NSString *)dateTimeString
{
    return [serverDateTimeFormater dateFromString:dateTimeString];
}

+(NSDate *)dateFromTokyoDateString:(NSString *)dateString
{
    return [serverDateFormater dateFromString:dateString];
}

+(NSString *)tokyoDatetimeStringFromDate:(NSDate *)date
{
    return [serverDateTimeFormater stringFromDate:date];
}

#pragma mark - Time
+(NSString *) timeFromFloatTime:(CGFloat)time
{
    NSInteger hour = (NSInteger)time;
    NSInteger minute = (time - hour)*60;
    
    return [NSString stringWithFormat:@"%02d:%02d",(int)hour,(int)minute];
}

+(CGFloat)timeFromSQLTimeString:(NSString *)timeString
{
    NSArray *components = [timeString componentsSeparatedByString:@":"];
    
    if (components.count == 3)
    {
        NSInteger hour = [[components objectAtIndex:0] integerValue];
        NSInteger minute = [[components objectAtIndex:1] integerValue];
        
        CGFloat time = hour + (CGFloat)minute/60;
        return time;
    }
    
    return NSNotFound;
}

+(NSString *)timeStringFromSQLTimeString:(NSString *)timeString
{
    CGFloat time = [self timeFromSQLTimeString:timeString];
    
    return [self timeFromFloatTime:time];
}

#pragma mark - Path
+(NSString *)documentPath
{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
}

#pragma mark - Lang
NSString *OMLang(NSString *key)
{
    return NSLocalizedString(key, key);
}
//replace "{KEY}" by "VALUE"
NSString *OMLangDict(NSString *key, NSDictionary *dict)
{
    NSString *str = NSLocalizedString(key, key);
    for (NSString *k in dict) {
        NSString *kPattern = [NSString stringWithFormat:@"{%@}", k];
        str = [str stringByReplacingOccurrencesOfString:kPattern
                                             withString:dict[k]];
    }
    return str;
}

#pragma mark - UUID
+(NSString *) genUUID
{
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString * uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
    CFRelease(newUniqueId);
    
    return uuidString;
}

@end

