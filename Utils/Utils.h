//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 alpha:1.0]

@interface Utils : NSObject
FOUNDATION_EXPORT void NSLog(NSString *format, ...) NS_FORMAT_FUNCTION(1,2) NS_NO_TAIL_CALL;
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ## __VA_ARGS__);

#pragma mark - Date
// Local timezone
+(NSString *) stringOnlyFromDate:(NSDate *)date;
+(NSDate *) dateFromDateString:(NSString *)dateString;
// Server timezone: +9
+(NSDate *) dateFromTokyoDateTimeString:(NSString *)dateTimeString;
+(NSDate *) dateFromTokyoDateString:(NSString *)dateString;
+(NSString *) tokyoDatetimeStringFromDate:(NSDate *)date;
#pragma mark - Time
+(NSString *) timeFromFloatTime:(CGFloat)time;
+(CGFloat) timeFromSQLTimeString:(NSString *)timeString;

/**
 Convert SQL Time string to Time String

 @param timeString SQL Time string. EX: 10:30:00
 @return NSString instance
 */
+(NSString *) timeStringFromSQLTimeString:(NSString *)timeString;
#pragma mark - Path
+(NSString *) documentPath;
#pragma mark - Lang
NSString *OMLang(NSString *key);
NSString *OMLangDict(NSString *key, NSDictionary *dict);
#pragma mark - UUID
+(NSString *) genUUID;
@end
